"""Module to hide the request for MarvelAPI

There are several classes to extend functionalities
for requests. Only the MarvelAPIRequest and 
DownloadCharactersImages are meant to be public.
@TODO: move base classes to another file

Depends on os to make the directory to save images
Depends on keymaker to access the keys to access the API
Depends on request to make the http requests to API 
"""

from LibAndFlask.customlib.keymaker import *
import os

class MarvelAPIBasePath:
    """Composes the Marver API path. This class is intended to be inherited."""

    main = 'http://gateway.marvel.com/v1/public'

    def __init__(self, mainFilter):
        """
        Initialize the Marvel API path with filters.

        mainFilter is the first accessor (after /public/)
        idFilter is the second accessor (after mainFilter)
        subFilter is the last accessor (after the idfilter)
        http://<blah...blah>/public/<mainFilter>
        http://<blah...blah>/public/<mainFilter>/<idFilter>
        http://<blah...blah>/public/<mainFilter>/<idFilter>/<subFilter>
        """
        
        self.mainFilter = mainFilter
        self.idFilter = None
        self.subFilter = None

    def __str__(self):
        """Generate the url"""
        urlcomps = [self.mainFilter, self.idFilter, self.subFilter]
        ret = MarvelAPIBasePath.main
        for u in urlcomps:
            if u is None:
                break
            ret += '/'+str(u)
               
        return ret

class MarvelAPIBaseFactories:
    """
    This class is intended to add factory methods
    to the path descriptors, which must inherit from it.
    
    The from<filter>Id methods are the factories to generate
    paths corresponding to its name like in fromStoryId:
    http://<apiaddress>/v1/public/Story/Id/<filter>
    """
    
    """Pathstring must be initialized with the
    mainFilter name of the descriptors"""
    PathString = ""

    @classmethod
    def fromStoryId(cls, storyId):
        ret = StoriesPath(storyId)
        ret.subFilter = cls.PathString
        return ret

    @classmethod
    def fromCharacterId(cls, characterId):
        ret = CharactersPath(characterId)
        ret.subFilter = cls.PathString
        return ret



class CharactersPath(MarvelAPIBasePath, MarvelAPIBaseFactories):
    """Descriptor for characters"""
    
    PathString = 'characters'

    def __init__(self, id = None):
        super().__init__(CharactersPath.PathString)
        self.idFilter = id

class StoriesPath(MarvelAPIBasePath, MarvelAPIBaseFactories):
    """Descriptor for stories"""
    
    PathString = 'stories'

    def __init__(self, id = None):
        super().__init__(StoriesPath.PathString)
        self.idFilter = id

class MarvelAPIRequest:
    """Public class to access the API"""

    import requests as req

    def __init__(self):
        self.authentication = HTTPGetVisitorPrivateHash.visit(KeyMaker.makePrivateHash())

    def CharacterById(self, id):
        """request character data from a specific ID."""
        
        data = self.authentication.copy()
        return MarvelAPIRequest.req.get(CharactersPath(id), data)

    def SomeStoryFromRocketRaccoon(self, excludeid = None):
        """request one of the best stories from RocketRacoon."""

        import random
        validIds = [90729, 96992, 96996, 96998, 97000]
        data = self.authentication.copy()
        return MarvelAPIRequest.req.get(StoriesPath(random.choice(validIds)), data)

    def RandomStoryFromRocketRaccoon(self):
        """request a random story from RocketRacoon."""

        import random
        data = self.authentication.copy()
        data['offset'] = random.randint(0,73)
        data['limit'] = 1
        data['orderBy'] = '-id'
        return MarvelAPIRequest.req.get(StoriesPath.fromCharacterId(1010744), data)

    def CharactersFromStory(self, storyId):
        """request characters from a story."""

        data = self.authentication.copy()
        data['limit'] = 100
        return MarvelAPIRequest.req.get(CharactersPath.fromStoryId(storyId), data)

    def DownloadThumb(self, thumbpath):
        """Download the image from thumbnail data."""

        data = self.authentication.copy()
        path = thumbpath['path'] + '.' + thumbpath['extension']
        return MarvelAPIRequest.req.get(path, data).content

class CharactersImageDownloader:
    """Specialized class to download images."""

    @staticmethod
    def downloadCharactersImagesFromStory(storyId):
        """
        Given an story ID, download all images to a static place.
        storyId can be a single integer, referencing an story id
        or can be the resulting json from a characters query.
        """
        con = MarvelAPIRequest()
        if isinstance(storyId, dict):
            chars = storyId
        elif isinstance(storyId, int):
            chars = con.CharactersFromStory(storyId).json()
        else:
            raise Exception("type not expected for storyId")

        if chars['code'] != 200:
            raise Exception('failed to fectch characters from story: '+str(storyId)+' from '+CharactersPath.fromStoryId(storyId))
        ids = chars['data']['results']
        for id in ids:
            imgJson = id['thumbnail']
            img = con.DownloadThumb(imgJson)
            filepath = './LibAndFlask/static/imgs/characters/'+str(id['id'])+'.'+imgJson['extension']
            os.makedirs(os.path.dirname(filepath), exist_ok=True)
            try:
                with open(filepath, 'xb') as imgf:
                    imgf.write(img)
            except:
                pass
        return ids

