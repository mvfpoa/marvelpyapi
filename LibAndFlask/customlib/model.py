""" Module: Storage for the website data

This module has the data needed to render the
application.

User must instanciate only one time, or
import it into a singleton if the instance
is needed globally.

It depends on json to be saved on/read from
disk for new requests. It keeps a local copy from
Marvel API requested data, to avoid requesting
from API all the time.
"""

import json

class Model:
    """Keeps data persistent."""
    
    def __init__(self, **kwargs):
        """Initialize it parameterless."""
        
        self.storyId = kwargs.get('storyId')
        self.storyTitle = kwargs.get('storyTitle')
        self.storyDescription = kwargs.get('storyDescription')
        self.copyright = kwargs.get('copyright')
        self.characters = []
        chparse = kwargs.get('characters', {})
        for c in chparse:
            self.characters.append(Model.Characters(**c))

    class Characters:
        """Using class alows to access model data easyly."""
    
        def __init__(self, **kwargs):
            """Shall not be instanciated, use the model method."""
            self.characterId = kwargs.get('characterId')
            self.characterName = kwargs.get('characterName')
            self.characterImage = kwargs.get('characterImage')

    def addCharacter(self, id, name, imgextension):
        """Use this method to create character and append it to model."""
        c = Model.Characters()
        c.characterId = id
        c.characterName = name
        c.characterImage = 'imgs/characters/'+str(id)+'.'+imgextension
        self.characters.append(c)

    def Save(self):
        """Save the model after altering/composing it."""
        temp = self.characters
        self.characters = []
        for t in temp:
            self.characters.append(t.__dict__)
        json.dump(self.__dict__, open('LibAndFlask/static/currentmodel.json', 'wt'))
        self.characters = temp

    @classmethod
    def Load(cls):
        """Load the single instance from disk."""
        data = json.load(open('LibAndFlask/static/currentmodel.json', 'rt+'))
        return cls(**data)

