
from LibAndFlask.customlib.model import Model

def testSaveLoadModel():
    m = Model()
    m.storyId = 100
    m.storyTitle = "Crazy Story #1"
    m.copyright = "© 2019"
    m.storyDescription = "Some extended description!"
    m.addCharacter(101, "CrazyGuy", 'jpg')
    m.addCharacter(102, "Dr.", 'jpg')
    m.Save()

    m2 = Model.Load()
    assert m2.storyId == 100
    assert m2.storyTitle == "Crazy Story #1"
    assert m2.storyDescription == "Some extended description!"
    assert m2.copyright == "© 2019"
    assert m2.characters[1].characterId == 102
    assert m2.characters[1].characterName == "Dr."
    assert m2.characters[1].characterImage == 'imgs/characters/102.jpg'
