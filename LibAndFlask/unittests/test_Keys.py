
from LibAndFlask.customlib.keymaker import (KeyMaker,PrivateHash)

def testLoadKeys():
    assert(KeyMaker.publicKey() is not None)
    assert(KeyMaker.privateKey() is not None)

def testMakePrivateHash():
    ph1 = KeyMaker.makePrivateHash()
    ph2 = KeyMaker().makePrivateHash()

    assert(ph1.timestamp != ph2.timestamp)
    assert(int(ph2.timestamp) - int(ph1.timestamp) >= 1)

