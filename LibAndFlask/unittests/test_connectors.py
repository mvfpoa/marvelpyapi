
from LibAndFlask.customlib.keymaker import KeyMaker, PrivateHash, HTTPGetVisitorPrivateHash
from LibAndFlask.customlib.MarvelAPI import MarvelAPIRequest, CharactersImageDownloader

def testRemoteConnection():
    con = MarvelAPIRequest()
    res = con.SomeStoryFromRocketRaccoon()
    assert(res.json()['code'] == 200)

def testCharactersImageDownload():
    import os
    os.remove("./LibAndFlask/static/imgs/characters/1010744.jpg")
    CharactersImageDownloader.downloadCharactersImagesFromStory(50616)
    try:
        with open("./LibAndFlask/static/imgs/characters/1010744.jpg","rb"):
            assert True
    except:
        assert False
