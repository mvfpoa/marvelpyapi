$(
    // this function corrects malformed strings due to problems with encoding
    function()
    {
        $('#description')[0].innerHTML = $('#description').text().replace(/â€™/g,"'");
        $('#description')[0].innerHTML = $('#description').text().replace(/â€¦/g,",");
    },

    // start bootstrap popper on lists
    $('li.startpopover').popover({
        html: true,
        trigger: 'hover',
        container: 'body',
        placement: 'top',
        content: getimg
    })

)

function getimg()
{
    img=$('#'+this.dataset['id']);
    return img.html();
}

