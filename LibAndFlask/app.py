
import os
import glob
from LibAndFlask.customlib.MarvelAPI import CharactersImageDownloader, MarvelAPIRequest
from LibAndFlask.customlib.model import Model
from flask import Flask, render_template, redirect, url_for

app = Flask(__name__)


@app.route('/')
def home():
    try:
        m = Model.Load()
    except:
        # redirect to download
        return redirect(url_for('load'))

    return render_template(
        "home_tmpl.html", **m.__dict__)


@app.route('/load')
def load():
    m = Model()
    con = MarvelAPIRequest()
    res = con.SomeStoryFromRocketRaccoon(excludeid=m.storyId).json()
    story = res['data']['results'][0]
    m.storyId = story['id']
    m.storyTitle = story['title']
    m.storyDescription = story['description']
    m.copyright = res['attributionHTML']

    chars = CharactersImageDownloader.downloadCharactersImagesFromStory(
        m.storyId)
    for c in chars:
        m.addCharacter(c['id'], c['name'], c['thumbnail']['extension'])

    m.Save()

    return redirect(url_for('home'))
    # route to home


@app.route('/clear')
def clear():
    fileList = glob.glob(
        './LibAndFlask/static/imgs/characters/*', recursive=False)
    for f in fileList:
        os.remove(f)
    os.remove('./LibAndFlask/static/currentmodel.json')
    return redirect(url_for('load'))


@app.route('/model')
def model():
    return app.send_static_file('currentmodel.json')
