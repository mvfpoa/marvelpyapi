# Marvel API classes and demo with Flask!

There are a lot of work to be done to make this a product!

# Setup

* Download the project
* Tested only on **python3**
* (optional) In the project's root, start `venv`
* Install packages listed at **requirements.txt**
    * **python3-flask** should be enough
* Go to **LibAndFlask/customlib/** and `cp keys_template.py keys.py`
* Edit **keys.py** and include yours MarvelAPI keys

## VSCode
* Open VSCode and open the project root folder
* Configure UnitTests with pytest at the LibAndFlask directory
* Run all tests:
    * Test whether it can read the keys
    * Test whether it can access content at MarvelAPI
    * Test whether model is accessible
* Set debugger to Flask (its already configured)
* Run debugger

## Bash
* change directory to project root directory
* run: `pytest LibAndFlask`
* if all tests is ok, you are ready to run the application
    * `./startdevelopment.sh`

## View the result
* Access in your browser http://127.0.0.1:5000/load to load the first content
* After loaded, the browser will be redirected to / to display the content
* If you want new content, access http://127.0.0.1:5000/load

# Site Notes
The site is like a periodic news, where the client can read periodically some fun fact about Rocket Racoon from Marvel TM.

There are 74 stories with Rocket Racoon at Marvel API, but only 5 has a long description to render. The site only consume those 5.

Thanks to Marvel API http://developer.marvel.com/

# License

The code is not under a specific license. Please talk to me before any change/copy/publication

# Looking for a lost template?

*keys.py*
```python
public=b"<insert your hex public key here>"
private=b"<insert your hex private key here>"
```
