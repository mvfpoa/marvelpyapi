#/bin/env sh

env APPLICATION_ROOT=./LibAndFlask/ FLASK_APP=./LibAndFlask/app.py FLASK_ENV=development FLASK_DEBUG=0 PYTHONIOENCODING=UTF-8 PYTHONUNBUFFERED=1 python -m flask run
